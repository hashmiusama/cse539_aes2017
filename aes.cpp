#include <iostream>
#include <algorithm>
#include "aeskeygen.h"
#include "lookups.h"
#include "aes.h"
#include "common.h"

using namespace std;

// method declarations for basic methods required for AES
void SubBytes(unsigned char*, bool);
void ShiftRows(unsigned char*, bool);
void ShiftOnce(unsigned char*, int);
void MixColumns(unsigned char*, bool);


// this is the main AES method that takes in given parameters and returns the encrypted message
// v is for verbose. if v sent in is true, it will display all the steps it uses for encryption
// if v is false, this method will not print anything to console.
// the length of key inputted is in bits
unsigned char* AES(int keyLen, const unsigned char *key, const unsigned char *input, bool v){

    // this helps seed the randomly timed function
    srand(static_cast<unsigned int>(time(nullptr)));
    // length of key in bytes
    int keyLengthBytes = keyLen/8;

    // getRounds function returns the number of rounds depending on the key size
    // as specified in the document
    int numberOfRounds = getRounds(keyLengthBytes);

    // state is 1D array which is programmed to behave like a 2D array
    // to avoid the complexity added in C++ when dealing with multidimensional array
    // required for handling pointers for each array
    auto *state = new unsigned char[16];

    // this method sets up the state using the input
    Setup(state, input, v);

    // this method XORs the state with key, byte by byte XOR is done
    AddRoundKey(state, key, v);

    // adding minimal randomness to the running time of the algorithm
    // to provide safety against timing attacks
    randomTimedLoop(v);

    // this methods takes uses the key to generate round keys for each round
    unsigned char* allKeys = keyExpansion(key, keyLengthBytes, v);

    auto *fState = new unsigned char[16];
    // loop for rounds
    // we have ensure that this runs 16 rounds irrespective of the given key length
    // so that side channel attacks such as timing attacks do not work on the encryption
    // all other methods run for a fixed amount of time
    for(int rounds = 0; rounds < 16; rounds++){
        // this method substitutes each byte in state with precomputed sbox values
        SubBytes(state, v);

        // this method shifts the each row by the index it is on
        // i.e first row is on zeroth index, it does not get shifted
        // second row is shifted one time
        // third row is shifted two times and fourth, three times
        ShiftRows(state, v);

        // adding minimal randomness to the running time of the algorithm
        // to provide safety against timing attacks
        randomTimedLoop(v);

        // this method does matrix multiplication of state
        // with matrix [[02 03 01 01],[01 02 03 01],[01 01 02 03], [03 01 01 02]]
        // on Galois Field 2, i.e matrix multiplication of binary bits
        // the XOR'ing instead of addition, which behaved exactly like adding
        MixColumns(state, v);

        // moving round key pointer to the desired round key depending on round
        unsigned char* roundKey = allKeys + blockLength + blockLength*rounds;

        // XORing the round key with state, byte by byte
        AddRoundKey(state, roundKey, v);

        // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
        roundKey = nullptr;
        delete roundKey;

        if(rounds == numberOfRounds - 2){
            for(int i = 0; i < 16; i++){
                fState[i] = state[i];
            }
        }
    }
    // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
    state = nullptr;
    delete state;

    // calling Sub bytes for the last time
    SubBytes(fState, v);

    // calling shift rows for the last time
    ShiftRows(fState, v);

    // adding minimal randomness to the running time of the algorithm
    // to provide safety against timing attacks
    randomTimedLoop(v);

    //getting the last round key
    unsigned char* roundKey = allKeys + blockLength + blockLength*(numberOfRounds-1);

    // XOR'ing the last round key byte by byte with state
    AddRoundKey(fState, roundKey, v);

    // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
    roundKey = nullptr;
    delete roundKey;

    // converting from state i.e a matrix representation of input into output
    auto *output = new unsigned char[16];
    Setup(output, fState, v);

    fState = nullptr;
    delete fState;

    return output;
}

// this method left shifts each row its index times
void ShiftRows(unsigned char *state, bool v){
    for(int i = 0; i < 4; i++){
        for(int j = 0; j < i; j++){
            ShiftOnce(state, i);
        }
    }
    display(state, v);
}

// this method shifts the given row one time
void ShiftOnce(unsigned char *state, int row){
    unsigned char temp = state[4*row];
    for(int i = 4*row; i < 4*row+3; i++){
        state[i] = state[i+1];
    }
    state[4*row+3] = temp;
}

// this method performs the Galois Field 2 matrix multiplication
// of state with matrix [[02 03 01 01],[01 02 03 01],[01 01 02 03], [03 01 01 02]]
void MixColumns(unsigned char *state, bool v) {
    auto newState = new unsigned char[16];
    for(int i = 0; i < 16; i++){
        int row = i/4;
        int column = i%4;
        if(row == 0)
            newState[i] = GaloisFieldMul(state[column], 0x02) ^ GaloisFieldMul(state[column+4], 0x03) ^ state[column+8] ^ state[column+12];
        if(row == 1)
            newState[i] = state[column] ^ GaloisFieldMul(state[column+4], 0x02) ^ GaloisFieldMul(0x03, state[column+8]) ^ state[column+12];
        if(row == 2)
            newState[i] = state[column] ^ state[column+4] ^ GaloisFieldMul(0x02, state[column+8]) ^ GaloisFieldMul(0x03, state[column+12]);
        if(row == 3)
            newState[i] = GaloisFieldMul(0x03, state[column]) ^ state[column+4] ^ state[column+8] ^ GaloisFieldMul(0x02, state[column+12]);
    }
    for(int i = 0; i < 16; i++)
        state[i] = newState[i];
    newState = nullptr;
    delete newState;
    display(state, v);
}

// This replaces the value for each byte of state with Rjindael Sbox value
void SubBytes(unsigned char *state, bool v){
    for(int i=0; i< 16; i++){
        state[i] = sbox[state[i]];
    }
    display(state, v);
}