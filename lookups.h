//
// Created by gaurav on 11/24/17.
//

#ifndef KEYGENTEST_LOOKUPS_H
#define KEYGENTEST_LOOKUPS_H

extern unsigned char sbox[256];
extern unsigned char inv_sbox[256];
extern unsigned char rcon[256];

#endif //KEYGENTEST_LOOKUPS_H
