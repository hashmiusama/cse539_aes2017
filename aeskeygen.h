//
// Created by gaurav on 11/23/17.
//

#ifndef CSE539_AES2017_AESKEYGEN_H
#define CSE539_AES2017_AESKEYGEN_H

unsigned char subWord(unsigned char k);
unsigned char* genRandomKey(int keyLength);
unsigned char* keyExpansion(const unsigned char key[], int keyLengthBytes, bool verbose);

#endif //CSE539_AES2017_AESKEYGEN_H
