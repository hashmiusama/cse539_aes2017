//
// Created by cogneto on 11/25/17.
//

#include "cbc.h"
#include "aes.h"
#include "aes_dec.h"
#include "aeskeygen.h"
#include "common.h"
#include <iostream>

/*
 * Copies size number of bytes from src to dest
 */
// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// CTR52-CPP. Guarantee that library functions do not overflow
// -- Explanation: This is called by our cbc encryption and decryption functions and we've taken care that src and dest
//    are of at least size bytes
void copyBytes(const unsigned char src[], unsigned char dest[], unsigned long size){

    for(int i=0; i< size; i++){
        dest[i] = src[i];
    }

}

/*
 * Xors size number of byte in src1 and src2 and saves output in dest
 */

// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// CTR52-CPP. Guarantee that library functions do not overflow
// -- Explanation: This is called by our cbc encryption and decryption functions and we've taken care that src1, src2 and dest
//    are of at least size bytes
void xorBytes(const unsigned char* src1, const unsigned char* src2, unsigned char* dest,  unsigned long size){

    for(int i=0; i<size; i++){
        dest[i] = src1[i] ^ src2[i];
    }

}

// Calculate number of padding bytes needed
// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// -- Explanation: mLength%(blockLength) will always be < blockLength so this function will never return -ve values
//    and thus will never cause problems below when we use the returned values as indices.
unsigned long getPaddingLength(unsigned long mLength){

    // Number of padding bytes needed is the number needed to make mLength divisible by blocklength
    return blockLength-mLength%(blockLength);
}

/*
 *  Takes a CBC decrypted message as input and verifies if the decryption is a valid PKCS#7 padded message.
 */

// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// CTR52-CPP. Guarantee that library functions do not overflow
// -- Explanation: This is called by our cbc encryption and decryption functions and we've taken care that decrypted_mLength
//    is the actual length of decrypted_m
bool verifyPadding(const unsigned char* decrypted_m, unsigned long decrypted_mLength){

    bool valid=true;

    // Padding bytes should all be equal to the padding length. So last byte is always the padding length
    unsigned long paddingLength = decrypted_m[decrypted_mLength-1];
    // If paddingLength is greater than block length, that's invalid.
    if(paddingLength>blockLength){
        return false;
    }
    // Since we know the padding length we can calculate the index of last message byte like below
    unsigned long lastMessageByteIndex = decrypted_mLength-paddingLength;

    // For padding to be valid all bytes between the last message byte and the last decrypted message byte should be
    // equal to the padding length
    for (unsigned long i=decrypted_mLength-2; i>lastMessageByteIndex; i--){

        if((int) decrypted_m[i] != paddingLength){
            valid = false;
            break;
        }
    }

    return valid;

}

/*
 *  Takes a CBC decrypted message as input. Assumes its a valid PKCS#7 padding and return message after padding removal
 */
// CTR50-CPP. Guarantee that container indices and iterators are within the valid range
// CTR52-CPP. Guarantee that library functions do not overflow
// -- Explanation: This is called by our cbc encryption and decryption functions and we've taken care that decrypted_mLength
//    is the actual length of decrypted_m
unsigned char* removePadding(unsigned char* decrypted_m, unsigned long decrypted_mLength){

    unsigned long paddingLength = decrypted_m[decrypted_mLength-1];
    unsigned long lastMessageByteIndex = decrypted_mLength-paddingLength;

    // PKCS#7 padding means that all padding bytes are equal to padding length. So the last byte in decrypted message is
    // always padding length. Using that we can get the last byte of the actual message and so remove the padding
    auto m = new unsigned char[lastMessageByteIndex];
    copyBytes(decrypted_m, m, lastMessageByteIndex);

    return m;

}

/*
 * Given message, key and their respective lengths, returns a cipher which is the CBC mode AES encryption of the message
 */

unsigned char* ENC_AES_CBC(int keyLength, unsigned char key[], unsigned char m[], unsigned long mLength, bool verbose){

    if(verbose) {
        std::cout << "Key: ";
        displayHex(key, static_cast<unsigned long>(keyLength / 8));
    }

    // Generating a random IV
    unsigned char* iv = genRandomKey(blockLength);

    if(verbose) {
        std::cout << "Message Length: " << mLength << std::endl;
        std::cout << "IV: ";
        displayHex(iv, static_cast<unsigned long>(blockLength));
    }

    // Calculate the length of padding needed
    unsigned long paddingLength = getPaddingLength(mLength);
    if(verbose) {
        std::cout << std::dec << "Padding Length: " << paddingLength << std::endl;
    }

    // Creating a bigger array for padded message and copying the message to it
    auto padded_m = new unsigned char[mLength+paddingLength];
    copyBytes(m, padded_m, mLength);

    // Adding padding bytes
    for(unsigned long i=mLength; i< mLength+paddingLength; i++){
        padded_m[i] = static_cast<unsigned char>(paddingLength);
    }
    if(verbose) {
        displayHex(padded_m, mLength + paddingLength);
    }

    // Cipher length has to be one block more than message length to include the IV
    // New message length is the original message length plus padding length
    auto cipher = new unsigned char[blockLength+mLength+paddingLength];

    // AES encrypts one block at a time.
    // currBlock points to the current message block we are encrypting
    unsigned char* currBlock = padded_m;
    unsigned long numBlocks=(mLength+paddingLength)/(blockLength);

    // Temporary variables used to store intermediate values
    auto temp1 = new unsigned char[blockLength];
    unsigned char* temp2;

    // c0 = iv
    unsigned char * cipher_index=cipher;
    for (int i=0; i<blockLength; i++){
        cipher[i]=iv[i];
        cipher_index = cipher_index + 1;
    }

    // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
    // MEM53-CPP. Explicitly construct and destruct objects when manually managing object lifetime
    // -- Explanation: iv is copied to cipher so no longer needed.
    iv = nullptr;
    delete iv;

    if(verbose) {
        std::cout << "After copying IV" << std::endl;
    }

    // CBC-AES encryption follows ci = AES(ci-1 xor mi) for each message block i
    for(int i=0; i< numBlocks; i++){

        if(verbose) {
            std::cout << "CurrBlock: " << std::endl;
            displayHex(currBlock, static_cast<unsigned long>(blockLength));
            std::cout << "Prev Cipher: " << std::endl;
            displayHex(cipher_index - blockLength, static_cast<unsigned long>(blockLength));
        }

        // Xor Ci-1 and mi. That is current message block with previous cipher block
        // cipher_index points to the location in the cipher array where the encryption of current block should go
        // So previous cipher is cipher_index - blocklength

        // CTR52-CPP. Guarantee that library functions do not overflow
        xorBytes(currBlock, cipher_index-blockLength, temp1, static_cast<unsigned long>(blockLength));
        if(verbose) {
            std::cout << "For message block number " << i << " ci-1 xor mi is: " << std::endl;
            displayHex(temp1, static_cast<unsigned long>(blockLength));
        }

        // AES Encryption for the output of previous step
        temp2 = AES(keyLength, key, temp1, true);

        // CTR52-CPP. Guarantee that library functions do not overflow
        copyBytes(temp2, cipher_index, static_cast<unsigned long>(blockLength));

        // Incrementing cipher_index and currBlock for the next loop
        cipher_index = cipher_index +blockLength;
        currBlock = currBlock+blockLength;

        if(verbose) {
            std::cout << "Cipher index after one iter: " << std::endl;
            displayHex(cipher_index - blockLength, static_cast<unsigned long>(blockLength));
        }
    }

    // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
    // MEM53-CPP. Explicitly construct and destruct objects when manually managing object lifetime
    // -- Explanation: Both of them are no longer needed.
    temp1 = nullptr;
    padded_m = nullptr;
    delete temp1;
    delete padded_m;

    return cipher;

}

/*
 * Given cipher, key and their respective lengths. Assuming the cipher was encrypted using CBC mode AES, returns the decrypted message.
 */

unsigned char* DEC_AES_CBC(int keyLength, unsigned char key[], unsigned char cipher[], unsigned long cipherLength, bool verbose){

    if(verbose) {
        std::cout << "Decryption function started" << std::endl << "Key: ";
        displayHex(key, static_cast<unsigned long>(keyLength / 8));
    }
    unsigned long blockLength=16;

    // Checking Cipher and blocklength are divisible
    if(cipherLength%blockLength !=0){
        throw std::runtime_error("Cipher Text Length not a multiple of blocklength. Something is wrong with the encryption.");
    }

    // Number of message blocks not counting the IV block. Since c0 = IV
    unsigned long numMessageBlocks = cipherLength/blockLength -1;
    if(verbose) {
        std::cout << "Cipher Length: " << std::dec << cipherLength << " Number of message blocks: " << numMessageBlocks
                  << std::endl;
    }

    auto m = new unsigned char[numMessageBlocks*blockLength];

    // Some arrays to store the intermediate values
    unsigned char* temp1;
    auto temp2 = new unsigned char[blockLength];


    // m_index points to the location in m where the current decryption will be stored
    unsigned char* m_index = m;
    // prev_cipher points to the previous cipher block
    unsigned char* prev_cipher = cipher;

    if(verbose) {
        std::cout << "IV/C0 : ";
        displayHex(prev_cipher, blockLength);
    }

    //cipher will point to the current cipher block
    cipher = cipher +blockLength;

    for(int i=0; i< numMessageBlocks; i++){

        if(verbose) {
            std::cout << "Current Cipher block before decryption : ";
            displayHex(cipher, blockLength);
        }

        // mi = Fk_inv(ci) xor ci-1. Here Fk is AES so Fk_inv is AES Decryption
        // temp1 stores the value of Fk_inv(ci)

        // CTR52-CPP. Guarantee that library functions do not overflow
        // -- Explanation: cipher points to start of current block so there's always 1 blocklength available
        temp1 = AES_DEC(keyLength, key, cipher, true);
        if(verbose) {
            std::cout << "ci-1 xor mi is: " << std::endl;
            displayHex(temp1, blockLength);
        }

        //  Xor decryption with ci-1 to get mi
        // CTR52-CPP. Guarantee that library functions do not overflow
        xorBytes(temp1, prev_cipher, temp2, blockLength);
        copyBytes(temp2, m_index, blockLength);

        // Incrementing pointers for the next loop
        m_index = m_index+blockLength;
        prev_cipher = prev_cipher+blockLength;
        cipher = cipher +blockLength;
    }

    if(verbose) {
        std::cout << "Decrypted Message: " << std::endl;
        displayHex(m, numMessageBlocks*blockLength);
    }

    // The Decrypted message will be padded. We need to remove the padding.
    // Asuming PKCS#7 padding
    // Message with padding removed will be stored in original_m
    // If padding is invalid error will be thrown
    unsigned char* original_m;
    if(verifyPadding(m, cipherLength - blockLength)){

        original_m = removePadding(m, cipherLength - blockLength);

    }else{

        throw std::runtime_error("Bad Padding");
    }

    // EXP51-CPP. Do not delete an array through a pointer of the incorrect type
    // MEM53-CPP. Explicitly construct and destruct objects when manually managing object lifetime
    // -- Explanation: Both of them are no longer needed.
    m= nullptr;
    temp2 = nullptr;
    delete m;
    delete temp2;

    return original_m;

}
